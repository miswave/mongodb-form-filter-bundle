<?php

namespace Miswave\Bundle\MongoDBFormFilterBundle\Event;

/**
 * Available filter events.
 */
class FilterEvents
{
    public const PREPARE = 'miswave_filter.prepare';
}
