<?php

namespace Miswave\Bundle\MongoDBFormFilterBundle;

use Miswave\Bundle\MongoDBFormFilterBundle\DependencyInjection\Compiler\FormDataExtractorPass;
use Miswave\Bundle\MongoDBFormFilterBundle\DependencyInjection\MiswaveMongoDBFormFilterExtension;
use Symfony\Component\DependencyInjection\Compiler\PassConfig;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class MiswaveMongoDBFormFilterBundle extends Bundle
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new FormDataExtractorPass(), PassConfig::TYPE_BEFORE_OPTIMIZATION, 0);
    }

    public function getContainerExtension(): ?ExtensionInterface
    {
        return new MiswaveMongoDBFormFilterExtension();
    }
}
