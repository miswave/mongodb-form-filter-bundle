<?php

namespace Miswave\Bundle\MongoDBFormFilterBundle\Tests\Fixtures\Filter;

use Miswave\Bundle\MongoDBFormFilterBundle\Filter\Form\Type\NumberFilterType;
use Miswave\Bundle\MongoDBFormFilterBundle\Filter\Form\Type\TextFilterType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Form filter for tests.
 */
class OptionFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('label', TextFilterType::class);
        $builder->add('rank', NumberFilterType::class);
    }

    /**
     * @return string
     */
    public function getBlockPrefix(): string
    {
        return 'options_filter';
    }
}
