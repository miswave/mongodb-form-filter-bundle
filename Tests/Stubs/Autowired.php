<?php

namespace Miswave\Bundle\MongoDBFormFilterBundle\Tests\Stubs;

use Miswave\Bundle\MongoDBFormFilterBundle\Filter\FilterBuilderUpdaterInterface;

class Autowired
{
    private FilterBuilderUpdaterInterface $filterBuilderUpdater;

    public function __construct(
        FilterBuilderUpdaterInterface $filterBuilderUpdater
    ) {
        $this->filterBuilderUpdater = $filterBuilderUpdater;
    }

    public function getFilterBuilderUpdater(): FilterBuilderUpdaterInterface
    {
        return $this->filterBuilderUpdater;
    }
}
