<?php

namespace Miswave\Bundle\MongoDBFormFilterBundle\Filter\Form;

use Miswave\Bundle\MongoDBFormFilterBundle\Filter\Form\Type;
use Miswave\Bundle\MongoDBFormFilterBundle\Filter\Form\Type\BooleanFilterType;
use Miswave\Bundle\MongoDBFormFilterBundle\Filter\Form\Type\CheckboxFilterType;
use Miswave\Bundle\MongoDBFormFilterBundle\Filter\Form\Type\ChoiceFilterType;
use Miswave\Bundle\MongoDBFormFilterBundle\Filter\Form\Type\CollectionAdapterFilterType;
use Miswave\Bundle\MongoDBFormFilterBundle\Filter\Form\Type\DateFilterType;
use Miswave\Bundle\MongoDBFormFilterBundle\Filter\Form\Type\DateRangeFilterType;
use Miswave\Bundle\MongoDBFormFilterBundle\Filter\Form\Type\DateTimeFilterType;
use Miswave\Bundle\MongoDBFormFilterBundle\Filter\Form\Type\DateTimeRangeFilterType;
use Miswave\Bundle\MongoDBFormFilterBundle\Filter\Form\Type\NumberFilterType;
use Miswave\Bundle\MongoDBFormFilterBundle\Filter\Form\Type\NumberRangeFilterType;
use Miswave\Bundle\MongoDBFormFilterBundle\Filter\Form\Type\SharedableFilterType;
use Miswave\Bundle\MongoDBFormFilterBundle\Filter\Form\Type\TextFilterType;
use Symfony\Component\Form\AbstractExtension;

/**
 * Load all filter types.
 */
class FilterExtension extends AbstractExtension
{
    /**
     * @return array
     */
    protected function loadTypes(): array
    {
        return [new BooleanFilterType(), new CheckboxFilterType(), new ChoiceFilterType(), new DateFilterType(), new DateRangeFilterType(), new DateTimeFilterType(), new DateTimeRangeFilterType(), new NumberFilterType(), new NumberRangeFilterType(), new TextFilterType(), new CollectionAdapterFilterType(), new SharedableFilterType()];
    }

    /**
     * {@inheritdoc}
     */
    public function loadTypeExtensions(): array
    {
        return [
            new FilterTypeExtension(),
        ];
    }
}
