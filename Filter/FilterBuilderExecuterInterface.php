<?php

namespace Miswave\Bundle\MongoDBFormFilterBundle\Filter;

use Miswave\Bundle\MongoDBFormFilterBundle\Filter\Query\QueryInterface;

interface FilterBuilderExecuterInterface
{
    /**
     * Add a join.
     *
     * @param string   $join
     * @param string   $alias
     * @param \Closure $callback
     */
    public function addOnce($join, $alias, \Closure $callback = null);

    /**
     * @return string
     */
    public function getAlias();

    /**
     * @return RelationsAliasBag
     */
    public function getParts();

    /**
     * @return QueryInterface
     */
    public function getFilterQuery();
}
