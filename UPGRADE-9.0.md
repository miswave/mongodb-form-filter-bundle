UPGRADE FROM previous to 9.0
=============================

You need to search and replace all namespace in your code:

before

```php
Lexik\Bundle\FormFilterBundle
```

after

```php
Miswave\Bundle\MongoDBFormFilterBundle
```


In `bundles.php`:

before

```php
use Lexik\Bundle\FormFilterBundle\LexikFormFilterBundle;

...

SpiriitFormFilterBundle::class => ['all' => true],
```

after

```php
use Miswave\Bundle\MongoDBFormFilterBundle\MiswaveMongoDBFormFilterBundle;

...

MiswaveMongoDBFormFilterBundle::class => ['all' => true],
```


In `spiriit_form_filter.yaml` / `spiriit_form_filter.php` bundle configuration file:

before

```php
spiriit_form_filter
```

after

```php
miswave_mongodb_form_filter
```

